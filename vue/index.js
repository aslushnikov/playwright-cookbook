const fs = require('fs');
const path = require('path');

const injectedScript = fs.readFileSync(path.join(__dirname, './injected.js'), 'utf8');

function parseSelector(selector) {
  selector = selector.trim();
  return selector;
}

function installSelectors(playwright) {
  const selectors = new Function(`
    ${injectedScript}
    ${parseSelector.toString()}
    return {
      query: (root, selector) => {
        console.log(selector);
        const {name, props} = parseSelector(selector);
        return queryAll(root, name, props)[0] || null;
      },
      queryAll: (root, selector) => {
        console.log(selector);
        const {name, props} = parseSelector(selector);
        return queryAll(root, name, props);
      },
    };
  `);
  playwright.selectors.register('vue', selectors);
}

module.exports = {
  installSelectors,
};
