function getComponentName(reactElement) {
  if (typeof reactElement.type === 'function')
    return reactElement.type.displayName || reactElement.type.name;
  if (typeof reactElement.type === 'string')
    return reactElement.type;
  return null;
}

function getProps(reactElement) {
  const props = reactElement.memoizedProps;
  if (!props || typeof props === 'string')
    return props;

  const returnProps = { ...props };
  delete returnProps.children;
  return returnProps;
}

function buildComponentsTree(reactElement) {
  const treeNode = {
    name: getComponentName(reactElement),
    props: getProps(reactElement),
    children: [],
    domNodes: [],
  };

  for (let child = reactElement.child; child; child = child.sibling)
    treeNode.children.push(buildComponentsTree(child));

  const stateNode = reactElement.stateNode;
  if (stateNode instanceof HTMLElement || stateNode instanceof Text) {
    treeNode.domNodes.push(stateNode);
  } else {
    for (const child of treeNode.children)
      treeNode.domNodes.push(...child.domNodes);
  }
  return treeNode;
}

function filterComponentsTree(treeNode, searchFn, result = []) {
  if (searchFn(treeNode))
    result.push(treeNode);
  for (const child of treeNode.children)
    filterComponentsTree(child, searchFn, result);
  return result;
}

function findReactRootOrDie() {
  const walker = document.createTreeWalker(document);
  while (walker.nextNode()) {
    if (walker.currentNode.hasOwnProperty('_reactRootContainer'))
      return walker.currentNode._reactRootContainer._internalRoot.current;
    for (const key of Object.keys(walker.currentNode)) {
      if (key.startsWith('__reactInternalInstance') || key.startsWith('__reactFiber'))
        return walker.currentNode[key];
    }
  }
  return null;
}

function isSubset(obj, subset) {
  if (subset === undefined)
    return true;
  if (typeof obj === 'object' && typeof subset === 'object') {
    for (const key of Object.keys(subset)) {
      if (!isSubset(obj[key], subset[key]))
        return false;
    }
    return true;
  }
  return obj === subset;
}

function queryAll(domScope, name, propsSubset = undefined) {
  const reactRoot = findReactRootOrDie();
  if (!reactRoot)
    return [];
  const tree = buildComponentsTree(reactRoot);
  console.log(tree);
  const treeNodes = filterComponentsTree(tree, treeNode => {
    if (treeNode.name !== name)
      return false;
    if (treeNode.domNodes.some(domNode => !domScope.contains(domNode)))
      return false;
    if (!isSubset(treeNode.props, propsSubset))
      return false;
    return true;
  });
  const allDOMNodes = new Set();
  for (const treeNode of treeNodes) {
    for (const domNode of treeNode.domNodes)
      allDOMNodes.add(domNode);
  }
  return [...allDOMNodes];
}

