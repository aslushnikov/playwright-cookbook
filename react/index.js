const fs = require('fs');
const path = require('path');

const injectedScript = fs.readFileSync(path.join(__dirname, './injectedScript.js'), 'utf8');

const decoratedReact = Symbol('decoratedReact');

function parseSelector(selector) {
  selector = selector.trim();
  const originalSelector = selector;
  const eat = (regex) => {
    const result = selector.match(regex);
    if (result === null)
      throw new Error(`Failed to parse selector "${originalSelector}"`);
    selector = selector.substring(result[0].length).trim();
    return result[1];
  }

  const name = eat(/^([\w]+)/);
  let props = undefined;
  if (selector)
    props = JSON.parse(selector);
  return {name, props};
}

function ensurePlaywrightReact(playwright) {
  if (playwright[decoratedReact])
    return;
  playwright[decoratedReact] = true;

  const selectors = new Function(`
    ${injectedScript}
    ${parseSelector.toString()}
    return {
      query: (root, selector) => {
        console.log(selector);
        const {name, props} = parseSelector(selector);
        return queryAll(root, name, props)[0] || null;
      },
      queryAll: (root, selector) => {
        console.log(selector);
        const {name, props} = parseSelector(selector);
        return queryAll(root, name, props);
      },
    };
  `);

  playwright.selectors.register('react', selectors);
}

module.exports = {
  installSelectors: ensurePlaywrightReact,
};
