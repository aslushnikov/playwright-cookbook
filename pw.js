const pw = require('playwright');

require('./react').installSelectors(pw);
require('./vue').installSelectors(pw);
require('./angular').installSelectors(pw);

async function fillReact(frame) {
  await frame.fill('react=NewBook >> react=input', 'asdf');
  await frame.click('react=NewBook >> react=button');
}

async function fillVue(frame) {
  throw new Error('now supported');
}

async function fillAngular(frame) {
  await frame.fill('ng-model=newBookName', 'asdf');
  await frame.click('text=new book');
}

(async () => {
  const browser = await pw.chromium.launch({headless: false, devtools: true});
  const context = await browser.newContext();
  const page = await context.newPage();
  await page.goto(`file://${__dirname}/static/index.html`);
  for (const frame of page.frames()) {
    if (!frame.parentFrame())
      continue;
    let frameColor = '#c8e6c9';
    try {
      if (frame.url().includes('react'))
        await fillReact(frame);
      else if (frame.url().includes('vue'))
        await fillVue(frame);
      else if (frame.url().includes('angular'))
        await fillAngular(frame);
      else throw new Error('Failed to automate!');
    } catch (e) {
      console.error(e);
      frameColor = '#ffcdd2';
    }
    await page.evaluate(({iframe, color}) => iframe.style.background = color, {
      iframe: await frame.frameElement(),
      color: frameColor,
    });
  }
})();

