import it from '@playwright/test';

it('should work', async({page}) => {
  it.setTimeout(45000);
  let time = 0;
  setInterval(() => console.log(++time, 'seconds'), 1000);
  await page.goto('https://aslushnikov.com');
  await page.click('button');
  console.log('done');
});

/*
test('should work', async ({page}) => {
  await page.goto('http://juliemr.github.io/protractor-demo/');
  await page.press('ng-model=first', '1');
  await page.press('ng-model=second', '2');
  await page.click('#gobutton');
  await page.click('react-component=MyComponent');
  await page.click('react-props="{foo: bar, baz: 10}"');
  await page.click('react-state="{foo: bar}"');

  await page.ng.waitForNG();
  expect(await page.textContent('ng-binding=latest')).toBe('3');
});
*/
