const fs = require('fs');
const path = require('path');

const clientSideScripts = require('./clientsidescripts.js');

async function executeScriptAsync(page, script, ...scriptArgs) {
  await page.evaluate(`
    new Promise((resolve, reject) => {
      const callback = (errMessage) => {
        if (errMessage)
          reject(new Error(errMessage));
        else
          resolve();
      };
      (function() {${script}}).apply(null, [...${JSON.stringify(scriptArgs)}, callback]);
    })
  `);
}

async function waitForAngular(page) {
  await executeScriptAsync(page, clientSideScripts.waitForAngular, '');
}

function installSelectors(playwright) {
  playwright.selectors.register('ng-model', new Function(`
    const findByModel = function() {
      ${clientSideScripts.findByModel}
    }
    return {
      query: (root, selector) => findByModel(selector, root, '')[0] || null,
      queryAll: (root, selector) => findByModel(selector, root, ''),
    };
  `));
  playwright.selectors.register('ng-binding', new Function(`
    const findBindings = function() {
      ${clientSideScripts.findBindings}
    }
    return {
      query: (root, selector) => findBindings(selector, false, root, '')[0] || null,
      queryAll: (root, selector) => findBindings(selector, false, root, ''),
    };
  `));
  playwright.selectors.register('ng-exact-binding', new Function(`
    const findBindings = function() {
      ${clientSideScripts.findBindings}
    }
    return {
      query: (root, selector) => findBindings(selector, true, root, '')[0] || null,
      queryAll: (root, selector) => findBindings(selector, true, root, ''),
    };
  `));
  /*
  playwright.selectors.register('ng-button-text', new Function(`
    const findByButtonText = function() { 
      ${clientSideScripts.findByButtonText}
    }
    return {
      query: (root, selector) => findByButtonText(selector, root)[0] || null,
      queryAll: (root, selector) => findByButtonText(selector, root),
    };
  `));
  playwright.selectors.register('ng-partial-button-text', new Function(`
    const findByPartialButtonText = function() { 
      ${clientSideScripts.findByPartialButtonText}
    }
    return {
      query: (root, selector) => findByPartialButtonText(selector, root)[0] || null,
      queryAll: (root, selector) => findByPartialButtonText(selector, root),
    };
  `));
  */
}

module.exports = {
  installSelectors,
};

